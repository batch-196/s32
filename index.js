let http = require("http");

//mock collection of courses:

let courses = [

	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn React",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}

];

http.createServer(function(request,response){

	console.log(request.url);//differentiate requests via endpoints
	console.log(request.method);//differentiate requests with http methods
	/*
		HTTP Requests are differentiated not only via endpoints but also with their methods.

		HTTP Methods simply tells the server what action it must take or what kind of response is needed for the request.

		With an HTTP methods we can actually create routes with the same endpoint but with different methods.

	*/

	if(request.url === "/" && request.method === "GET"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. GET method request.");

	} else if (request.url === "/" && request.method === "POST"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. POST method request.");

	} else if (request.url === "/" && request.method === "PUT"){

		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("This is the / endpoint. PUT method request");

	} else if (request.url === "/" && request.method === "DELETE"){

		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("This is the / endpoint. DELETE method request");

	} else if (request.url === "/courses" && request.method === "GET"){

		//When sending a JSON format data as response, change the Content-Type of the response to application/json
		response.writeHead(200,{'Content-Type': 'application/json'});
		response.end(JSON.stringify(courses));
		//stringify as JSON our courses array to send it as a response

	} else if (request.url === "/courses" && request.method === "POST"){
		//Route to add a new course, we have to receive an input from the client.

		//To be able to receive the request body or the input from the request, we have to add a way to received that input.

		//In NodeJS, this is done in 2 steps.

		//requestBody will be a placeholder to contain the data (request body) passed from the client.
		let requestBody = "";

		//1st step in receiving data from the request in NodeJS is called the data step.
		//data step - will read the incoming stream of data from the client and process it so we can save it in the requestBody variable

		request.on('data',function(data){

			//console.log(data);//stream of data from client
			requestBody += data;
			//data stream is saved into the variable as a string

		})

		//end step - this will run once or after the request data has been completely sent from the client:

		request.on('end',function(){

			//We have completely received the data from the client and thus requestBody now contains are our input
			//console.log(requestBody);

			//Initially, requestBody is in JSON format. We cannot add this to our courses array because it's a string. So, we have to update requestBody variable with a parsed version of the received JSON format data.
			requestBody = JSON.parse(requestBody);

			//console.log(requestBody);//requestBody is now an object

			//Add the requestBody in the array.
			courses.push(requestBody);

			//check the courses array if we were able to add our requestBody
			//console.log(courses);

			response.writeHead(200,{'Content-Type': 'application/json'});
			//Send the updated courses array to the client as JSON format.
			response.end(JSON.stringify(courses));	

		})


	}



}).listen(4000);

console.log("Server running at localhost:4000");


/*
	Mini Activity

	Create 2 new routes for the following endpoint: "/courses"

	1. This route should be a "GET" method route
		-Add our status code, 200 and our Content Type: text/plain.
		-Add an end() method to end the response with the following message:
		"This is a response to a GET method request for the /courses endpoint."
	
	2. This route should be a "POST" method route
		-Add our status code, 200 and our Content Type: text/plain.
		-Add an end() method to end the response with the following message:
		"This is a response to a POST method request for the /courses endpoint."

	-Use Postman and create request appropriately to test your routes.
	-Make sure to restart your server first.

*/